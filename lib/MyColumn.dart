import 'package:flutter/material.dart';

import 'constants.dart';

class MyColumn extends StatelessWidget {
  final String? logoName;
  final IconData? iconcuk;

  MyColumn({this.logoName, this.iconcuk});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(
          iconcuk,
          size: 60,
          color: Colors.black54,
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          logoName!,
          style: kMetinStili,
        )
      ],
    );
  }
}
